import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ListaPostsPage } from '../pages/listaPosts/listaPosts';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';
import { PagesFilteredPostsPage } from '../pages/pages-filtered-posts/pages-filtered-posts';
import { PostCommentsPage } from '../pages/post-comments/post-comments';
import { AddPostPage } from '../pages/add-post/add-post';
import { EditPostPage } from '../pages/edit-post/edit-post';
import { DeletePostPage } from '../pages/delete-post/delete-post';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    MyApp,
    ListaPostsPage,
    ItemDetailsPage,
    ListPage,
    PagesFilteredPostsPage,
    PostCommentsPage,
    AddPostPage,
    EditPostPage,
    DeletePostPage
  ],
  imports: [
    ComponentsModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListaPostsPage,
    ItemDetailsPage,
    ListPage,
    PagesFilteredPostsPage,
    PostCommentsPage,
    AddPostPage,
    EditPostPage,
    DeletePostPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClientModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
