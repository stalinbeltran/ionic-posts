import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { ListaPostsPage } from '../pages/listaPosts/listaPosts';
import { PagesFilteredPostsPage } from '../pages/pages-filtered-posts/pages-filtered-posts';
import { PostCommentsPage } from '../pages/post-comments/post-comments';
import { AddPostPage } from '../pages/add-post/add-post';
import { EditPostPage } from '../pages/edit-post/edit-post';
import { DeletePostPage } from '../pages/delete-post/delete-post';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make ListaPostsPage the root (or first) page
  rootPage = ListaPostsPage;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Posts list', component: ListaPostsPage },
      { title: 'Filtered list', component: PagesFilteredPostsPage },
      { title: 'Post comments', component: PostCommentsPage },
      { title: 'Add post', component: AddPostPage },
      { title: 'Edit post', component: EditPostPage },
      { title: 'Delete post', component: DeletePostPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
