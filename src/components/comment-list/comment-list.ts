import { Component, Input, SimpleChanges  } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the CommentListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'comment-list',
  templateUrl: 'comment-list.html'
})
export class CommentListComponent {

  @Input() url:string;
  posts: any = [];
  constructor(private http: HttpClient) {
  }

  updateComments = function(){
    console.log('url:' + this.url)
    console.log(this.url)
    this.http.get(this.url).subscribe((response) => {
      this.posts = response
    });
    console.log('update comments')
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('cambios en commentsList')
    this.updateComments()
  }
}
