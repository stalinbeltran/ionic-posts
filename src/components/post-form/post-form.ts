import { Component, Input } from '@angular/core';

/**
 * Generated class for the PostFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'post-form',
  templateUrl: 'post-form.html'
})
export class PostFormComponent {

  text: string;
  @Input() post:object
  constructor() {
    console.log('Hello PostFormComponent Component');
    this.text = 'Hello World';
  }

}
