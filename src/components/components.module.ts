import { NgModule } from '@angular/core';
import { PostListComponent } from './post-list/post-list';
import { IonicApp, IonicModule } from 'ionic-angular';
import { ComponentsHeaderComponent } from './components-header/components-header';
import { CommentListComponent } from './comment-list/comment-list';
import { PostFormComponent } from './post-form/post-form';
@NgModule({
	declarations: [PostListComponent,
    ComponentsHeaderComponent,
    CommentListComponent,
    PostFormComponent],
	imports: [IonicModule],
	exports: [PostListComponent,
    ComponentsHeaderComponent,
    CommentListComponent,
    PostFormComponent]
})
export class ComponentsModule {}
