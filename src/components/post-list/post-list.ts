import { Component, Input, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the PostListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'post-list',
  templateUrl: 'post-list.html'
})
export class PostListComponent {
  @Input() url:string;
  posts: any = [];
  constructor(private http: HttpClient) {
  }

  updatePosts = function(){
    this.http.get(this.url).subscribe((response) => {
      this.posts = response
    });
  }

  ngOnInit()    {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.updatePosts()
    console.log('cambios en postList')
  }

}
