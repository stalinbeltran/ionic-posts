import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagesFilteredPostsPage } from './pages-filtered-posts';

@NgModule({
  declarations: [
    PagesFilteredPostsPage,
  ],
  imports: [
    IonicPageModule.forChild(PagesFilteredPostsPage),
  ],
})
export class PagesFilteredPostsPageModule {}
