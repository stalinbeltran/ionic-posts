import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PagesFilteredPostsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pages-filtered-posts',
  templateUrl: 'pages-filtered-posts.html',
})
export class PagesFilteredPostsPage {
  userId:string='2'
  urlBase:string = 'http://jsonplaceholder.typicode.com/posts?userId='
  private _url1 = this.urlBase + this.userId;
  public get url1() {
    return this._url1;
  }
  public set url1(value) {
    this._url1 = this.urlBase +  value;
    //this._url1 = value;
    console.log('set url1')
  }
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagesFilteredPostsPage');
  }

  mostrar(){
    console.log(this.userId)
    this.url1 = this.userId
  }

}
