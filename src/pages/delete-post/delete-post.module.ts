import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeletePostPage } from './delete-post';

@NgModule({
  declarations: [
    DeletePostPage,
  ],
  imports: [
    IonicPageModule.forChild(DeletePostPage),
  ],
})
export class DeletePostPageModule {}
