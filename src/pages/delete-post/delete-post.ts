import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-delete-post',
  templateUrl: 'delete-post.html',
})
export class DeletePostPage {

  urlBase:string = 'https://jsonplaceholder.typicode.com/posts/'
  post:object = {title:'ghgjhghgjh', body:'64464564', userId:'1', id:''}
  postId:string = '1'
  private _url1 = this.urlBase + this.postId;
  public get url1() {
    return this._url1;
  }
  public set url1(value) {
    console.log('set url1')
    this._url1 = this.urlBase +  value;
    this.loadPost('Post loaded')
    console.log(this._url1)
  }


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private http: HttpClient, private toastCtrl:ToastController) {
  }

  ngOnInit() {
    this.url1 = this.postId
    this.loadPost()
  }

  loadPost = function(message:string=undefined){
    this.http.get(this.url1).subscribe((response) => {
      console.log(response)
      this.post = response
      message && this.presentToast(message)
    });
  }

  deletePost = function(){
    this.http.delete(this.url1).subscribe((response) => {
      console.log('response updatepost')
      console.log(response)
      this.post = response
      this.presentToast('Post deleted!')
    });
  }


  presentToast = function(message) {
    //console.log(this.toastCtrl)
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  mostrar(){
    console.log(this.postId)
    this.url1 = this.postId
  }
}
