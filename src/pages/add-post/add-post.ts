import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the AddPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-post',
  templateUrl: 'add-post.html',
})
export class AddPostPage {
  url:string = 'http://jsonplaceholder.typicode.com/posts/'
  post:object = {title:'ghgjhghgjh', body:'64464564', userId:'1'}
  //toastCtrl:ToastController

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private http: HttpClient, private toastCtrl:ToastController) {
      //this.toastCtrl = toastCtrl
  }

  updatePost = function(){
    this.http.post(this.url).subscribe((response) => {
      console.log(response)
      this.post = response
      this.presentToast()
    });
  }

  presentToast = function() {
    console.log(this.toastCtrl)
    let toast = this.toastCtrl.create({
      message: 'Post saved!',
      duration: 3000
    });
    toast.present();
  }

}
