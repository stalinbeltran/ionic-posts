import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-listaPosts',
  templateUrl: 'listaPosts.html'
})
export class ListaPostsPage {
  response: Object;
  posts: any = [];
  url: string = 'http://jsonplaceholder.typicode.com/posts/';
  constructor(private http: HttpClient) {

  }

}
