import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the PostCommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-comments',
  templateUrl: 'post-comments.html',
})
export class PostCommentsPage {
  post:object = {title:'', body:''}
  postId:string='2'
  urlBase:string = 'http://jsonplaceholder.typicode.com/posts/'
  comments:any=[{name:'dfsdf', body:'sdfsdfsdf'}]
  private _url1 = this.urlBase + this.postId;
  private _urlComments = this.urlBase + 1 + '/comments';
  public get urlComments() {
    return this._urlComments;
  }
  public set urlComments(value) {
    this._urlComments = this.urlBase + value + '/comments';    
  }
  public get url1() {
    return this._url1;
  }
  public set url1(value) {
    console.log('value')
    console.log(value)
    this._url1 = this.urlBase +  value;
    console.log('set url1')
    console.log(this._url1)
    this.updatePost()
  }
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private http: HttpClient) {
  }

  updatePost = function(){
    this.http.get(this._url1).subscribe((response) => {
      console.log(response)
      this.post = response
      console.log(this.post.id)
      this.urlComments = this.post.id
      console.log('urlcomments')
      console.log(this.urlComments)
    });
  }

  mostrar(){
    console.log(this.postId)
    this.url1 = this.postId
  }

  ngOnInit() {
    this.url1 = this.postId
  }

}
